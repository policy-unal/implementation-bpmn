package co.edu.unal.gpn_camunda_unal.app.reactivacion.service;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

@Slf4j
public class NotificaPolizaYatieneProcesoService implements JavaDelegate {

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        var policyNumber = (String) delegateExecution.getVariable("policyNumber");

        log.info("""
                                
                P2: Notifica vía email a usuarios que la póliza ya se encuentra en  
                procesamiento de reactivación en otra instancia de proceso para la póliza %s""".formatted(policyNumber));

    }
}
