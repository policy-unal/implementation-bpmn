package co.edu.unal.gpn_camunda_unal.app.cancelacion.service;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

@Slf4j
public class ValidarEstadoCancelacionService implements JavaDelegate {

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        var policyNumber = (String) delegateExecution.getVariable("policyNumber");


        // alguna consulta a una BBDD o microservicio
        if (policyNumber.endsWith("999")) {
            // la poliza no existe
            delegateExecution.setVariable("polizaExiste", false);
        } else {
            delegateExecution.setVariable("polizaExiste", true);
        }

        log.info("""
                                
                P3: Validar vigencia de la póliza %s
                """.formatted(policyNumber));

    }
}
