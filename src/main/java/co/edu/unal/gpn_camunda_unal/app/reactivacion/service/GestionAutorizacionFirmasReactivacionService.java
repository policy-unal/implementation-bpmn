package co.edu.unal.gpn_camunda_unal.app.reactivacion.service;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

@Slf4j
public class GestionAutorizacionFirmasReactivacionService implements JavaDelegate {

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        var policyNumber = (String) delegateExecution.getVariable("policyNumber");

        log.info("""
                                
                P7: Se han gestionado las autorizaciones y firmas 
                para la reactivación de la póliza %s """
                .formatted(policyNumber));

    }
}
