package co.edu.unal.gpn_camunda_unal.app.pruebas.dto;

public record ProcessInstanceDto(String id,
                                 String processDefinitionId,
                                 boolean ended) {
}
