package co.edu.unal.gpn_camunda_unal.app.reactivacion.service;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

@Slf4j
public class ConfirmacionReactivacionAprobadaService implements JavaDelegate {

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        var policyNumber = (String) delegateExecution.getVariable("policyNumber");

        // logica que reactiva la póliza (status cancelled -> status active)

        log.info("""
                                
                P8: Se confirma que la reactivación  
                ha sido aprobada para la póliza %s""".formatted(policyNumber));

    }
}
