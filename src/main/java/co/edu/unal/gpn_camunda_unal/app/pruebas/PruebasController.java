package co.edu.unal.gpn_camunda_unal.app.pruebas;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.task.TaskQuery;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/cancellations")
@RequiredArgsConstructor
@Slf4j
public class PruebasController {

    private final RuntimeService runtimeService;

    private final TaskService taskService;

    @PostMapping("/start")
    public String startCancellation(@RequestParam String policyNumber,
                                    @RequestParam String customerNumber,
                                    @RequestParam boolean isPremium,
                                    @RequestParam int numeroPeriodosAnteriores) {

        log.info("Start cancellation process for policyNumber {}, customerNumber {}", policyNumber, customerNumber);

        runtimeService.startProcessInstanceByKey("cancellationProcess",
                Map.of("policyNumber", policyNumber,
                        "customerNumber", customerNumber,
                        "isPremium", isPremium,
                        "numeroPeriodosAnteriores", numeroPeriodosAnteriores));

        log.info("End start cancellation");

        return "Cancellation process started for policyNumber: "
                + policyNumber + " and customerNumber: " + customerNumber;
    }

    @GetMapping("/tareas")
    public String getAvailableTasks(@RequestParam(required = false) String solicitudId) {
        // Obtener las tareas disponibles
        TaskQuery taskQuery = taskService.createTaskQuery();
        if (solicitudId != null) {
            taskQuery.processInstanceId(solicitudId);
        }
        List<Task> tasks = taskQuery.list();

        log.info("Tareas {}, {},  {}", tasks.get(0).getDescription());

        return "Tareas %s, %s, %s".formatted(tasks.get(0).getDescription(),
                tasks.get(0).getId(),
                tasks.get(0).getAssignee());
    }

    @GetMapping("/advisors")
    public Set<String> getAvailableTasks() {
        // Obtener las tareas disponibles
        TaskQuery taskQuery = taskService.createTaskQuery();
        taskQuery.taskAssignee("advisors");
        List<Task> tasks = taskQuery.list();

        return tasks.stream().map(t -> "Id %s".formatted(t.getId())).collect(Collectors.toSet());

    }

    @GetMapping("/asesoresn2")
    public Set<String> getAvailableTasksAsesoresN2() {
        // Obtener las tareas disponibles
        TaskQuery taskQuery = taskService.createTaskQuery();
        taskQuery.taskAssignee("asesoresN2");
        List<Task> tasks = taskQuery.list();

        return tasks.stream().map(t -> "Id %s".formatted(t.getId())).collect(Collectors.toSet());

    }

    @PostMapping("/asesoresN2/aprobar/{taskId}")
    public ResponseEntity<String> aprobarPolizaN2(@PathVariable String taskId,
                                                  @RequestParam Boolean aprobado) {
        // Completar la tarea con los datos proporcionados
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        if (task != null) {
            taskService.complete(taskId, Map.of("esAprobadoPorNivelDos", aprobado));
            return ResponseEntity.ok("Tarea completada exitosamente");
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/verifyPolicy/{taskId}/verify")
    public ResponseEntity<String> verifyPolicy(@PathVariable String taskId, @RequestParam Boolean policyIsOk) {
        // Completar la tarea con los datos proporcionados
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        if (task != null) {
            taskService.complete(taskId, Map.of("policyIsValidForCancel", policyIsOk));
            return ResponseEntity.ok("Tarea completada exitosamente");
        } else {
            return ResponseEntity.notFound().build();
        }


    }

    @GetMapping("/solicitudes/{solicitudId}")
    public ResponseEntity<String> getVacationRequest(@PathVariable String solicitudId) {
        // Obtener los datos de la solicitud utilizando el ID del proceso
        String requestDto = (String) runtimeService.getVariable(solicitudId, "policyNumber");

        if (requestDto != null) {
            return ResponseEntity.ok(requestDto);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
