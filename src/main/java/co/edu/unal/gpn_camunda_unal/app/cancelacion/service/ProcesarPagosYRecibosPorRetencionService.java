package co.edu.unal.gpn_camunda_unal.app.cancelacion.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProcesarPagosYRecibosPorRetencionService implements JavaDelegate {

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        var policyNumber = (String) delegateExecution.getVariable("policyNumber");

        var customerNumber = (String) delegateExecution.getVariable("customerNumber");

        log.info("""
                                
                P10: El cliente %s ha aceptado la oferta de retención,
                se procesarán los pagos y recibos para la póliza %s.
                """.formatted(policyNumber, customerNumber));

    }
}
