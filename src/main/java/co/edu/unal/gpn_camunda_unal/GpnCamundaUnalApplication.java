package co.edu.unal.gpn_camunda_unal;

import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GpnCamundaUnalApplication {

	public static void main(String[] args) {
		SpringApplication.run(GpnCamundaUnalApplication.class, args);
	}

}
