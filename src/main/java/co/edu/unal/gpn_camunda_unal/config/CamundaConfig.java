package co.edu.unal.gpn_camunda_unal.config;


import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableProcessApplication
public class CamundaConfig {

   /* @Bean
    public ProcessEngine processEngine() {
        return new StandaloneProcessEngineConfiguration()
                .setJdbcUrl("jdbc:postgresql://192.168.215.2:5432/postgres?currentSchema=gpn-unal-app2")
                .setJdbcUsername("postgres")
                .setJdbcPassword("password")
                .setJdbcDriver("org.postgresql.Driver")
                .setDatabaseTablePrefix("camunda_")
                .setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE)
                .buildProcessEngine();
    }*/

}
